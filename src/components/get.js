const BASE_URL = 'https://api.stackexchange.com/2.2/';

function toQueryString(obj) {
    let parts = [];
    for (let i in obj) {
        if (obj.hasOwnProperty(i)) {
            parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
        }
    }
    return parts.join("&");
}

const get = (url, values) => {

    let fullUrl = `${BASE_URL}${url}?${toQueryString(values)}`;

    return fetch(fullUrl, {
        credentials: 'omit',
        mode: 'cors',
        method: 'get'
    }).then(response => Promise.all([response, response.json()]));
};

export default get;