import React from 'react';
import moment from 'moment';

const QuestionWindow = (props) => {
    const {item} = props;

    return (<div className="App__window-item">
        <h2 dangerouslySetInnerHTML={{__html: item.title}}/>
        <div className="App__item-content" dangerouslySetInnerHTML={{__html: item.body}}/>
        <div className="App__item-info">
            <div className="user">
                <i style={{backgroundImage: `url('${typeof(item.owner.profile_image) !== 'undefined' ? item.owner.profile_image : ''}')`}}/>
                <span>{item.owner.display_name}</span>
            </div>
            <div className="creation-date" title="Creation date">
                <i/>{moment.unix(item.creation_date).format('LLL')}
            </div>
            <div className="views" title="Views">
                <i/>{item.view_count}
            </div>
        </div>
    </div>)
};

export default QuestionWindow;
