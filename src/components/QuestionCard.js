import React from 'react';
import moment from 'moment';

const QuestionCard = (props) => {
    const {item, onClick} = props;

    return (<a className="App__item" onClick={onClick.bind(this, item.question_id)}>
        <h3 dangerouslySetInnerHTML={{__html: item.title}}/>
        <div className="App__item-info">
            <div className="user">
                <i style={{backgroundImage: `url('${typeof(item.owner.profile_image) !== 'undefined' ? item.owner.profile_image : ''}')`}}/>
                <span>{item.owner.display_name}</span>
            </div>
            <div className="creation-date" title="Creation date">
                <i/>{moment.unix(item.creation_date).format('LLL')}
            </div>
            <div className="views" title="Views">
                <i/>{item.view_count}
            </div>
        </div>
    </a>)
};

export default QuestionCard;
