import React, {Component} from 'react';

class SearchForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
            sort: 'creation',
            sortType: 'asc',
            textEmpty: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        if (!this.textValidation())
            return;

        this.props.onSubmit(this.state);
    }

    textValidation() {
        if (this.state.text.length > 0) {
            this.setState({
                textEmpty: false
            });
            return true;
        } else {
            this.setState({
                textEmpty: true
            });
            return false;
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {

        return (
            <form className="App__form" onSubmit={this.handleSubmit}>
                <input type="text"
                       name="text"
                       onChange={this.handleInputChange}
                       className={this.state.textEmpty ? `error` : ``}
                       placeholder="Search..."
                />
                <div className="App__select App__select--sort">
                    <select onChange={this.handleInputChange} name="sort">
                        <option value="creation">Sort by creation date</option>
                        <option value="relevance">Sort by relevance</option>
                    </select>
                </div>
                <div className="App__select App__select--sort-type">
                    <select onChange={this.handleInputChange} name="sortType">
                        <option value="asc">Order asc</option>
                        <option value="desc">Order desc</option>
                    </select>
                </div>
                <input className="button" type="submit" value="Search!"/>
            </form>
        );
    }
}

export default SearchForm;
