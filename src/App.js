import React, {Component} from 'react';
import './App.scss';
import SearchForm from './components/SearchForm';
import QuestionCard from './components/QuestionCard';
import ReactPaginate from 'react-paginate';
import get from './components/get';
import autoBind from 'react-autobind';
import Modal from 'react-modal';
import QuestionWindow from './components/QuestionWindow';

const config = {
    itemsPerPage: 20,
    searchFilter: '!-*jbN0CeyJHb',
    questionFilter: '!-*jbN-UV)TrL',
    site: 'stackoverflow'
};


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            resultsLoaded: false,
            results: {
                total: 0,
            },
            formData: {
                page: 1
            },
            loading: false,
            modalIsOpen: false,
            modalContent: null
        };

        autoBind(this);
    }

    load() {
        this.setState({loading: true});

        get('search', {
            page: this.state.formData.page,
            pagesize: config.itemsPerPage,
            order: this.state.formData.sortType,
            sort: this.state.formData.sort,
            intitle: this.state.formData.text,
            filter: config.searchFilter,
            site: config.site
        }).then(([response, json]) => {
            this.setState({results: json, resultsLoaded: true, loading: false});
            window.scrollTo(0, 0);
        }).catch(error => {
            this.setState({loading: false});
            alert('Request error! See more in console.');
            console.log(error);
        });
    }

    handleSubmit(data) {

        data.page = 1;

        this.setState({
            formData: data
        }, () => {
            this.load();
        });
    }

    handlePageClick(page) {

        let formData = this.state.formData;
        formData.page = page.selected + 1;

        this.setState({
            formData: formData
        }, () => {
            this.load();
        });
    }

    handleCardClick(id) {
        this.setState({loading: true});

        get(`questions/${id}`, {
            filter: config.questionFilter,
            site: config.site

        }).then(([response, json]) => {
            if (json.items.length > 0) {
                this.setState({
                    modalContent: json.items[0],
                    modalIsOpen: true,
                    loading: false
                });
            } else {
                this.setState({loading: false});
                alert('Request error! See more in console.');
                console.log(json);
            }
        }).catch(error => {
            this.setState({loading: false});
            alert('Request error! See more in console.');
            console.log(error);
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        });
    }

    render() {
        let {results, resultsLoaded, loading, formData} = this.state,
            items = null;

        if (results.total > 0) {
            items = results.items.map((item) => {
                return (<QuestionCard onClick={this.handleCardClick} key={item.question_id} item={item}/>);
            })
        }

        if (!items && resultsLoaded) {
            items = (<div className="no-results">No results</div>);
        }

        return (
            <div className={`App ${resultsLoaded ? `loaded` : ``}`}>
                <div className={`main-loading-pane ${loading ? `active` : ``}`}/>
                <SearchForm onSubmit={this.handleSubmit}/>
                <div className="App__items">
                    {items}
                </div>
                {results.total > 20 && <ReactPaginate previousLabel={""}
                                                      nextLabel={""}
                                                      breakLabel={<span>...</span>}
                                                      pageCount={Math.ceil(results.total / config.itemsPerPage)}
                                                      marginPagesDisplayed={2}
                                                      pageRangeDisplayed={5}
                                                      onPageChange={this.handlePageClick}
                                                      containerClassName={"App__pagination"}
                                                      activeClassName={"active"}
                                                      forcePage={formData.page - 1}/>}
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal}
                    defaultStyles={{}}
                    appElement={document.getElementById('root')}>

                    <i className="ReactModal__close" onClick={this.closeModal}/>
                    <QuestionWindow item={this.state.modalContent}/>
                </Modal>
            </div>
        );
    }
}

export default App;
