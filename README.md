#TEST APP
##Components
###SearchForm
####Props
onSubmit - form submit handler
###QuestionCard
Stateless component.
Item of searching result. It contains title and some info about the question: author, creation date and views.
####Props
data - object from api

onClick - function to opening windows with questions body
###QuestionWindow
Stateless component.

Item of searching result with details. It contains title, body and some info about the question: author, creation date and views.
####Props
item - object from api
##Local Deployment
Clone the repository and move into:
```
git clone https://ivg2009@bitbucket.org/ivg2009/test_app.git
cd test_app
```
Install dependencies:
```
npm install
```
Run the server:
```
npm start
```
Open your browser and go to http://localhost:3000/
##Online demo
https://demo.crystal-web.ru